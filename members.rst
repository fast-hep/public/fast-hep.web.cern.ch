Members
=======

In alphabetical order:

* `Olivier Davignon <mailto:davignon@cern.ch>`_ (CERN)
* `Luke Kreczko <mailto:L.Kreczko@bristol.ac.uk>`_ (University of Bristol)
* `Ben Krikler <mailto:b.krikler@bristol.ac.uk>`_ (University of Bristol)
They can be collectivelly reached at `fast-hep@cern.ch <mailto:fast-hep@cern.ch>`_.

Past members include: Jacob Linacre, Emmanuel Olaiya, Tai Sakuma.
