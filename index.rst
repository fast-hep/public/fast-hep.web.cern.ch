=============================================
Faster Analysis Software Taskforce (F.A.S.T.)
=============================================

F.A.S.T. is a group of High Energy Physics `scientists <members.html>`_ who:

* Contribute to existing software tools and/or develop its own `packages <packages.html>`_ to close gaps in the High Energy Physics software ecosystem.
* Develop and showcase an “ideal F.A.S.T. analysis” and demonstrate its capabilities and performance.
* Provide expertise on how to incorporate modern analysis techniques into analyses (eg. deep machine learning, vectorised array programming, etc), and provides feedback to the developers of such tools.
* Educate F.A.S.T. members and colleagues on what modern analysis tools and techniques are available and encourages knowledge sharing through presentations and workshops.

The current set of interacting packages is described in this figure: 

.. image:: images/package_overview-colour-description-all_packages.png
  :width: 600
  :alt: Alternative text
|
The `packages <packages.html>`_ are available on `pypi <https://pypi.org/user/fast-hep/>`_ and are implemented in `gitlab <https://gitlab.cern.ch/fast-hep/public/>`_. Installation can be achieved through, for example::

  pip install fast-carpenter
  pip install fast-plotter
  pip install fast-datacard

Who's using it
==============
F.A.S.T. tools are currently used by members of: the `CMS Collaboration <https://cms.cern/>`_ (including analysis and upgrade work), the `LZ Dark Matter Experiment <https://lz.lbl.gov/>`_, and the `Deep Underground Neutrino Experiment <https://www.dunescience.org/>`_ (DUNE). Collaborations or individuals that are interested in our tools or need support can reach us at `fast-hep@cern.ch <mailto:fast-hep@cern.ch>`_. We also have a community on gitter: https://gitter.im/FAST-HEP/community.

The tools are written in python and are powered by external libraries and tools: `pandas <https://pandas.pydata.org/>`_, `numpy <https://www.numpy.org/>`_, `matplotlib <https://matplotlib.org/>`_, ``scikit-hep`` (in particular `uproot <https://github.com/scikit-hep/uproot>`_), `alphatwirl <https://github.com/alphatwirl>`_, `atuproot <https://github.com/shane-breeze/atuproot>`_, etc.


Contents
========

.. toctree::
   :maxdepth: 2

   packages.rst
   members.rst
   talks.rst
.. goals.rst

