Setting up EOS for CERN's Gitlab CI
===================================


Setting up project website and deploying docs using gitlab CI
--------------------------------------------------------------
Following instructions from https://cern.service-now.com/service-portal/article.do?n=KB0003905 

- create a dedicated `service account`_ for publishing the files (in this case fasthep)
- set up `project website`_
   - using the service account, request project space on EOS (in this case /eos/project/f/fast-hep/): https://cern.service-now.com/service-portal/article.do?n=KB0003151 
   - https://cern.service-now.com/service-portal/report-ticket.do?name=EOS-projet-space&se=CERNBox-Service
   - in the ticket say that the space is for website deployment and give any unusual space requirements (the default seems to be 1 TB), e.g. https://cern.service-now.com/service-portal/view-request.do?n=RQF0958623
   - if others need to access the project space set up associated e-group permissions ( e.g. add fast-cms to cernbox-project-fast-hep-writers at https://e-groups.cern.ch )
   - http://cernbox.web.cern.ch/cernbox/en/project_space/manage_project_space.html
   - create folder for website on eos (in this case /eos/project/f/fast-hep/www/cms/fast-ra1/ )
   - create project website (using category “official website” and the root eos path, e.g. /eos/project/f/fast-hep/www): https://cern.ch/webservices/Services/CreateNewSite/
- then add any other managers to the list of moderators at https://webservices.web.cern.ch/webservices/Services/ManageSite/
- set up gitlab secret variables at https://gitlab.cern.ch/fast-cms/FAST-RA1/settings/ci_cd 
   - instructions at `deploy eos project`_
- update the .gitlab-ci.yml as follows: https://gitlab.cern.ch/fast-cms/FAST-RA1/commit/6b3f0d18b43465acc75684d4b48cfc6df027ab2a 
   - instructions at `deploy eos project`_



Using EOS space for private CI files
------------------------------------


.. _`service account`:  https://account.cern.ch/account/Management/NewAccount.aspx
.. _`project website`: https://cernbox.web.cern.ch/cernbox/en/web/project_website_content.html
.. _`deploy eos project`: https://gitlab.cern.ch/gitlabci-examples/deploy_eos
