How to configure your project for readthedocs.org
=================================================
Since readthedocs_ supports full github integration, this how-to will focus on the gitlab side.

 1. Create your project on gitlab
 2. Create a project with the same name on readthedocs_.
 3. Create a webhook, see https://docs.readthedocs.io/en/latest/webhooks.html
 4. Push something to the repository and a docs build should start

Other requirements

 * The gitlab project must be publicly available
 * the usual readthedocs constraints apply: https://docs.readthedocs.io/en/latest/getting_started.html


.. _readthedocs: https://readthedocs.org
